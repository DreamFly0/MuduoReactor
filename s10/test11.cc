/******************************************************
 *单元测试：新增定时器删除功能eventloop新增cancel函数；
 */
#include "EventLoop.h"
#include "../base/logging.h"
#include "TimerId.h"

#include <functional>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int cnt = 0;
EventLoop* g_loop;
TimerId toCancel;

void cancelSelf()
{
	printf("cancelSelf\n");
	g_loop->cancel(toCancel);
}

int main()
{
	Logger::setLogLevel(Logger::TRACE);
	EventLoop loop;
	g_loop = &loop;

	toCancel = loop.runEvery(3, std::bind(cancelSelf));

	loop.loop();
	printf("main loop exits");
}
