/************************************************************
 *client ---> TcpRelay(Tunnel) ----> server
 *Function:扮演类似proxy角色，作为客户端访问用户指定的server
 *其中类变量m_ptrSeverConn连接client,m_ptrClientConn连接server
************************************************************/
#pragma once

#include "../../base/logging.h"
#include "../../net/EventLoop.h"
#include "../../net/InetAddress.h"
#include "../../net/TcpClient.h"
#include "../../net/TcpServer.h"
#include "../../base/noncopyable.h"

#include <memory>
#include <functional>

using namespace placeholders;

class Tunnel : public enable_shared_from_this<Tunnel>,noncopyable
{
public:
	Tunnel(EventLoop* loop,
		const InetAddress& serverAddr,
		const TcpConnectionPtr& serverConn)
		:m_client(loop, serverAddr, serverConn->name()),
		m_ptrSeverConn(serverConn)
	{
		LOG_INFO << "Tunnel " << serverConn->peerAddress().toHostPort()
			<< " <-> " << serverAddr.toHostPort();
	}

	~Tunnel()
	{
		LOG_INFO << "~Tunnel";
	}

	void setup()
	{
		m_client.setConnectionCallback(
			std::bind(&Tunnel::onClientConnection, shared_from_this(), _1));
		m_client.setMessageCallback(
			std::bind(&Tunnel::onClientMessage,shared_from_this(),_1,_2,_3));
		m_ptrSeverConn->setHighWaterMarkCallback(
			std::bind(&Tunnel::onHighWaterMarkWeak,
			std::weak_ptr<Tunnel>(shared_from_this()), kServer, _1, _2), 1024 * 1024);
	}

	void connect()
	{
		m_client.connect();
	}

	void disconnect()
	{
		m_client.disconnect();
	}

	const TcpConnectionPtr& getClientConn() const { return m_ptrClientConn; }
private:
	void teardown()
	{
		m_client.setConnectionCallback(defaultConnectionCallback);
		m_client.setMessageCallback(defaultMessageCallback);
		if (m_ptrSeverConn)
		{
			m_ptrSeverConn->shutdown();
			//m_ptrServerConn.reset();
		}
		m_ptrClientConn.reset();
	}
	//作为client 连接Server
	void onClientConnection(const TcpConnectionPtr& conn)
	{
		LOG_INFO << "Client " << " is " << (conn->connected() ? "UP" : "DOWN");
		if (conn->connected())
		{
			conn->setTcpNoDelay(true);
			conn->setHighWaterMarkCallback(
				std::bind(&Tunnel::onHighWaterMarkWeak,
				std::weak_ptr<Tunnel>(shared_from_this()), kClient, _1, _2), 1024 * 1024);
			m_ptrSeverConn->startRead();
			m_ptrClientConn = conn;
			//将用户连接发来暂存的数据发送到服务器
			if (m_ptrSeverConn->inputBuffer()->readableBytes() > 0)
			{
				conn->send(m_ptrSeverConn->inputBuffer());
			}
		}
		else
		{
			teardown();
		}
	}

	void onClientMessage(const TcpConnectionPtr& conn,
		Buffer* buf,
		Timestamp)
	{
		LOG_DEBUG << conn->name() << " receives " << buf->readableBytes() << " Bytes";
		if (m_ptrSeverConn)
		{
			m_ptrSeverConn->send(buf);
		}
		else
		{
			buf->retrieveAll();
			abort();
		}
	}

	enum ServerClient
	{
		kServer, kClient
	};

	void onHighWaterMark(ServerClient which,
		const TcpConnectionPtr& conn,
		size_t bytesToSent)
	{
		LOG_INFO << (which == kServer ? "Server" : "Client")
			<< " onHighWaterMark " << conn->name()
			<< " bytes " << bytesToSent;
		if (which == kServer)
		{
			//server --> TcpRelay发送数据速率过快，TcpRelay --> client
			//来不及发送，停止读取server 发来 TcpRelay的数据
			if (m_ptrSeverConn->outputBuffer()->readableBytes() > 0)
			{
				m_ptrClientConn->stopRead();
				m_ptrSeverConn->setWriteCompleteCallback(
					std::bind(&Tunnel::onWriteCompleteWeak,
					std::weak_ptr<Tunnel>(shared_from_this()), kServer, _1));
			}
		}
		else
		{
			//情况与上述情况相反
			if (m_ptrClientConn->outputBuffer()->readableBytes() > 0)
			{
				m_ptrSeverConn->stopRead();
				m_ptrClientConn->setWriteCompleteCallback(
					std::bind(&Tunnel::onWriteCompleteWeak,
					std::weak_ptr<Tunnel>(shared_from_this()), kClient, _1));
			}
		}
	}

	//weak_ptr why?
	static void onHighWaterMarkWeak(const std::weak_ptr<Tunnel>& wkTunnel,
		ServerClient which,
		const TcpConnectionPtr& conn,
		size_t bytesToSent)
	{
		std::shared_ptr<Tunnel> tunnel = wkTunnel.lock();
		if (tunnel)
		{
			tunnel->onHighWaterMark(which, conn, bytesToSent);
		}
	}

	void onWriteComplete(ServerClient which, const TcpConnectionPtr& conn)
	{
		LOG_INFO << (which == kServer ? "server" : "client")
			<< " onWriteComplete " << conn->name();
		if (which == kServer)
		{
			//如果发送缓冲区数据发送完毕，则重新接受数据
			m_ptrClientConn->startRead();
			m_ptrSeverConn->setWriteCompleteCallback(WriteCompleteCallback());
		}
		else
		{
			m_ptrSeverConn->startRead();
			m_ptrClientConn->setWriteCompleteCallback(WriteCompleteCallback());
		}
	}

	static void onWriteCompleteWeak(const std::weak_ptr<Tunnel>& wkTunnel,
		ServerClient which,
		const TcpConnectionPtr& conn)
	{
		std::shared_ptr<Tunnel> tunnel = wkTunnel.lock();
		if (tunnel)
		{
			tunnel->onWriteComplete(which, conn);
		}
	}

	TcpClient m_client;
	TcpConnectionPtr m_ptrSeverConn;
	TcpConnectionPtr m_ptrClientConn;
};

typedef shared_ptr<Tunnel> TunnelPtr;