#include "tunnel.h"
#include "../../net/EventLoopThreadPool.h"
#include "../../base/singleton.h"

#include <malloc.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/resource.h>

EventLoop* g_eventLoop;
InetAddress* g_serverAddr;
std::map<string, TunnelPtr> g_tunnels;

void onServerConnection(const TcpConnectionPtr& conn)
{
	LOG_DEBUG << (conn->connected() ? "UP" : "DOWN");
	if (conn->connected())
	{
		
		conn->setTcpNoDelay(true);
		conn->stopRead();
		TunnelPtr tunnel(new Tunnel(g_eventLoop, *g_serverAddr, conn));
		tunnel->setup();
		tunnel->connect();
		g_tunnels[conn->name()] = tunnel;
	}
	else
	{
		assert(g_tunnels.find(conn->name()) != g_tunnels.end());
		g_tunnels[conn->name()]->disconnect();
		g_tunnels.erase(conn->name());
	}
}

void onServerMessage(const TcpConnectionPtr& conn, Buffer* buf, Timestamp)
{
	LOG_DEBUG << conn->localAddress().toHostPort() << " receive " << buf->readableBytes()
		<< " bytes from " << conn->localAddress().toHostPort();

	TunnelPtr tunnel = g_tunnels[conn->name()];
	if (tunnel && tunnel->getClientConn())
	{
		const TcpConnectionPtr& clientConn = tunnel->getClientConn();
		clientConn->send(buf);
	}
}
//显示本程序内存使用情况
void memstat()
{
	malloc_stats();
}

int main(int argc, char* argv[])
{
	Logger::setLogLevel(Logger::TRACE);
	if (argc < 4)
	{
		fprintf(stderr,"Usage: %s <host_ip> <port> <listen_port>\n",argv[0]);
	}
	else
	{
		//set max virtual memory to 256M
		size_t kOneMB = 1024 * 1024;
		rlimit rl = { 256 * kOneMB, 256 * kOneMB };
		setrlimit(RLIMIT_AS, &rl);
	}
	const char* ip = argv[1];
	uint16_t port = static_cast<uint16_t>(atoi(argv[2]));
	InetAddress serverAddr(ip, port);
	g_serverAddr = &serverAddr;

	uint16_t acceptPort = static_cast<uint16_t>(atoi(argv[3]));
	InetAddress listenAddr(acceptPort);
	
	EventLoop loop;
	g_eventLoop = &loop;
	memstat();
	loop.runEvery(5, memstat);

	Singleton<EventLoopThreadPool>::Instance().Init(&loop, 0);
	Singleton<EventLoopThreadPool>::Instance().start();

	TcpServer server(&loop, listenAddr, "TcpRelay");
	server.setConnCallback(onServerConnection);
	server.setMsgCallback(onServerMessage);

	server.start();
	loop.loop();
}