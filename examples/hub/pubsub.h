#pragma once

#include "../../net/TcpClient.h"

class PubSubClient
{
public:
	typedef std::function<void(PubSubClient*)> ConnectionCallback;
	typedef std::function<void(const string& topic,
		const string& content,
		Timestamp)> SubscribeCallback;
	PubSubClient(EventLoop* loop, const InetAddress& hubAddr, const string& name);
	void start();
	void stop();
	bool connected()const;
	void setConnCallback(const ConnectionCallback& cb){ m_connCallback = cb; }

	bool subscribe(const string& topic, const SubscribeCallback& cb);
	void unsubscribe(const string& topic);
	bool publish(const string& topic, const string& content);
private:
	void onConnection(const TcpConnectionPtr& conn);
	void onMessage(const TcpConnectionPtr& conn,
		Buffer* buf,
		Timestamp receiveTime);
	bool send(const string& message);

	TcpClient m_client;
	TcpConnectionPtr m_ptrConn;
	ConnectionCallback m_connCallback;
	SubscribeCallback m_subCallback;
};


