#pragma once

#include "../../net/Buffer.h"


#include <string>

namespace pubsub
{

enum ParseResult
{
	kError,
	kSuccess,
	kContinue,
};

ParseResult parseMessage(Buffer* buf,
	std::string* cmd,
	std::string* topic,
	std::string* content);
}
