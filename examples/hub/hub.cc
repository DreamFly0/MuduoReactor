/***************************************************
 *Function:应用层的一个广播协议，为简单起见，Hub实例采用"\r\n"分界的文本协议，这样用telnet(client)就能测试
 *协议只有三个命令：
 *订阅<topic> : "sub <topic>\r\n"
 *退订：		"unsub <topic>\r\n"
 *发送内容:     "pub <topic>\r\n<content>\r\n",任何一个client都可以发送内容，订阅此topic的Subscriber会都收到<content>
***************************************************/
#include "codec.h"

#include "../../base/logging.h"
#include "../../net/EventLoop.h"
#include "../../net/TcpServer.h"
#include "../../base/noncopyable.h"
#include "../../net/EventLoopThreadPool.h"
#include "../../base/singleton.h"

#include <functional>
#include <map>
#include <set>
#include <stdio.h>
using namespace placeholders;

namespace pubsub
{

typedef std::set<string> ConnectionSubscription;
	
class Topic
{
public:
	Topic(const string& topic) :m_strTopic(topic)
	{}

	void add(const TcpConnectionPtr& conn)
	{
		m_audiences.insert(conn);
		if (m_lastPubTime.valid())
		{
			conn->send(makeMessage());
		}
	}

	void remove(const TcpConnectionPtr& conn)
	{
		m_audiences.erase(conn);
	}

	void publish(const string& content, Timestamp time)
	{
		m_strContent = content;
		m_lastPubTime = time;
		string message = makeMessage();
		for (auto item : m_audiences)
		{
			item->send(message);
		}
	}

private:
	string makeMessage(){ return "TOPIC : " + m_strTopic + "\r\n" + m_strContent + "\r\n"; }

	string m_strTopic;
	string m_strContent;
	Timestamp m_lastPubTime;
	std::set<TcpConnectionPtr> m_audiences;		//每个topic维护一个订阅topic的TcpConnectionPtr集合
};

class PubSubServer : public noncopyable
{
public:
	PubSubServer(EventLoop* loop,
		const InetAddress& listenAddr)
		: m_pLoop(loop),
		m_server(loop, listenAddr, "PubSubServer")
	{
		m_server.setConnCallback(
			std::bind(&PubSubServer::onConnection, this, _1));
		m_server.setMsgCallback(
			std::bind(&PubSubServer::onMessage, this, _1, _2, _3));
		m_pLoop->runEvery(1.0, std::bind(&PubSubServer::timePublish, this));
	}

	void start(){ m_server.start(); }
private:

	void onConnection(const TcpConnectionPtr& conn)
	{
		if (conn->connected()){
			string connName = conn->name();
			m_mapConnectionSubscription[connName] = ConnectionSubscription();
		}
		else
		{
			string connName = conn->name();
			const ConnectionSubscription& connSub = m_mapConnectionSubscription[connName];
			
			for (ConnectionSubscription::const_iterator it = connSub.begin();
				it != connSub.end();)
			{
				// subtle: doUnsubscribe will erase *it, so increase before calling.
				doUnsubscribe(conn, *it++);
			}
			size_t n = m_mapConnectionSubscription.erase(connName);
			assert(1 == n);
		}
	}

	void onMessage(const TcpConnectionPtr& conn,
		Buffer* buf,
		Timestamp receiveTime)
	{
		ParseResult result = kSuccess;
		while (result == kSuccess)
		{
			string cmd;
			string topic;
			string content;
			result = parseMessage(buf, &cmd, &topic, &content);
			if (result == kSuccess)
			{
				if ("pub" == cmd)
				{
					duPublish(conn->name(), topic, content, receiveTime);
				}
				else if ("sub" == cmd){
					LOG_INFO << conn->name() << " subscribes " << topic;
					doSubscribe(conn,topic);
				}
				else if ("unsub" == cmd)
				{
					doUnsubscribe(conn, topic);
				}
				else
				{
					conn->shutdown();
					result = kError;
				}
			}
			else if (result == kError)
			{
				conn->shutdown();
			}
		}
	}

	void timePublish()
	{
		Timestamp now = Timestamp::now();
		duPublish("internal", "utc_time", now.toFormattedString(), now);
	}

	void doSubscribe(const TcpConnectionPtr& conn,
		const string& topic)
	{
		string connName = conn->name();
		ConnectionSubscription& connSub = m_mapConnectionSubscription[connName];
		connSub.insert(topic);
		getTopic(topic).add(conn);
	}

	void duPublish(const string& source,
		const string& topic,
		const string& content,
		Timestamp time)
	{
		getTopic(topic).publish(content, time);
	}

	void doUnsubscribe(const TcpConnectionPtr& conn,
		const string& topic)
	{
		LOG_INFO << conn->name() << " unsubscribes " << topic;
		getTopic(topic).remove(conn);
		// topic could be the one to be destroyed, so don't use it after erasing.
		string connName = conn->name();
		ConnectionSubscription& connSub = m_mapConnectionSubscription[connName];
		connSub.erase(topic);
	}

	Topic& getTopic(const string& topic)
	{
		std::map<string, Topic>::iterator it = m_mapTopic.find(topic);
		if (it == m_mapTopic.end())
		{
			//insert retval = pair<iterator,bool>
			it = m_mapTopic.insert(make_pair(topic, Topic(topic))).first;
		}
		return it->second;
	}
	EventLoop* m_pLoop;
	TcpServer m_server;
	std::map<string, Topic> m_mapTopic;
	std::map<string, ConnectionSubscription> m_mapConnectionSubscription;
};
}

int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
		EventLoop loop;
		Singleton<EventLoopThreadPool>::Instance().Init(&loop, 0);
		Singleton<EventLoopThreadPool>::Instance().start();
		pubsub::PubSubServer server(&loop, InetAddress(port));
		server.start();
		loop.loop();
	}
	else
	{
		printf("Usage: %s pubsub_port [inspect_port]\n", argv[0]);
	}
}