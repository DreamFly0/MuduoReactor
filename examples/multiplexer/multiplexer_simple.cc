/**************************************************
 *Fuction:单线程"串并转换"服务器，这个连接服务器把多个客户连接汇聚为一个内部TCP连接
 *MultiplexServer收集各个连接的数据，打包发给后端服务器
************************************************/

#include "../../base/logging.h"
#include "../../net/EventLoop.h"
#include "../../net/TcpServer.h"
#include "../../base/noncopyable.h"
#include "../../net/EventLoopThreadPool.h"
#include "../../base/singleton.h"
#include "../../net/TcpClient.h"

#include <functional>
#include <mutex>
#include <map>
#include <queue>

#include <stdio.h>

using namespace placeholders;

const size_t kMaxPacketLen = 255;
const size_t kHeaderLen = 3;
const int kMaxConns = 10;  // 65535

const uint16_t kClientPort = 3333;
const char* backendIp = "127.0.0.1";
const uint16_t kBackendPort = 9999;

class MultiplexServer : public noncopyable
{
public:
	MultiplexServer(EventLoop* loop, const InetAddress& listenAddr, const InetAddress& backendAddr)
		:m_server(loop, listenAddr, "MultiplexServer"),
		m_backend(loop, backendAddr, "MultiplexBackend")
	{
		m_server.setConnCallback(std::bind(&MultiplexServer::onClientConnection, this, _1));
		m_server.setMsgCallback(std::bind(&MultiplexServer::onClientMessage, this, _1, _2, _3));
		m_backend.setConnectionCallback(std::bind(&MultiplexServer::onBackendConnection, this, _1));
		m_backend.setMessageCallback(std::bind(&MultiplexServer::onBackendMessage, this, _1, _2, _3));
		m_backend.enableRetry();
	}
	void start()
	{
		m_backend.connect();
		m_server.start();
	}
private:
	void onClientConnection(const TcpConnectionPtr& conn)
	{
		LOG_INFO << "Client " << conn->peerAddress().toHostPort() << " -> "
			<< conn->localAddress().toHostPort() << " is "
			<< (conn->connected() ? "UP" : "DOWN");
		if (conn->connected())
		{
			int id = -1;
			//为避免id过快被复用（造成backend串话），采用queue管理id，每次从queue头部取id，用完放到尾部
			if (!m_availIds.empty())
			{
				id = m_availIds.front();
				m_availIds.pop();
			}
			if (id <= 0)
			{
				conn->shutdown();
			}
			else
			{	//有连接通知backend
				string connName = conn->name();
				m_mapConnId[connName] = id;
				m_mapClientConn[id] = conn;
				char buf[256];
				snprintf(buf,sizeof buf,"CONN %d FROM %s IS UP\r\n",
					id,conn->peerAddress().toHostPort().c_str());
				sendBackendString(0, buf);
			}
		}
		else
		{
			if (m_mapConnId.count(conn->name()) > 0)
			{	//连接下线通知backend
				int id = m_mapConnId[conn->name()];
				assert(id > 0 && id <= kMaxConns);
				char buf[256];
				snprintf(buf, sizeof buf, "CONN %d FROM %s IS DOWN\r\n",
					id, conn->peerAddress().toHostPort().c_str());
				sendBackendString(0, buf);

				if (m_backendConnn)
				{
					//put client id back for reusing
					m_availIds.push(id);
					m_mapClientConn.erase(id);
				}
				else
				{
					assert(m_availIds.empty());
					assert(m_mapClientConn.empty());
				}
			}
		}
	}

	//收到来自connection的消息，将其打包发送到backend Server,
	void onClientMessage(const TcpConnectionPtr& conn, Buffer* buf, Timestamp)
	{
		if (m_mapConnId.count(conn->name()) > 0)
		{
			int id = m_mapConnId[conn->name()];
			sendBackendBuffer(id, buf);
		}
		else
		{
			buf->retrieveAll();
			// FIXME: error handling
		}
	}

	void sendBackendBuffer(int id, Buffer* buf)
	{
		while (buf->readableBytes() > kMaxPacketLen)
		{
			Buffer packet;
			packet.append(buf->peek(), kMaxPacketLen);
			buf->retrieve(kMaxPacketLen);
			sendBackendPacket(id, &packet);
		}

		if (buf->readableBytes() > 0)
		{
			sendBackendPacket(id, buf);
		}
	}

	//multiplexerServer作为client连接backend Server
	void onBackendConnection(const TcpConnectionPtr& conn)
	{
		LOG_INFO << "Backend " << conn->localAddress().toHostPort() << " ->"
			<< conn->peerAddress().toHostPort() << " is "
			<< (conn->connected() ? "UP" : "DOWN");
		if (conn->connected())
		{
			//第一次连接初始化id队列
			m_backendConnn = conn;
			assert(m_availIds.empty());
			for (int i = 1; i <= kMaxConns; ++i)
			{
				m_availIds.push(i);
			}
		}
		else
		{
			//如果断开连接，则断开所有与multiplexerServer的connection
			m_backendConnn.reset();
			for (auto item : m_mapClientConn)
			{
				item.second->shutdown();
			}
			m_mapClientConn.clear();
			while (!m_availIds.empty())
			{
				m_availIds.pop();
			}
		}
	}

	//收到backend服务器的消息根据消息头部的id转发给特定的connection
	void onBackendMessage(const TcpConnectionPtr& conn, Buffer* buf, Timestamp)
	{
		sendtoClient(buf);
	}

	void sendtoClient(Buffer* buf)
	{
		while (buf->readableBytes() > kHeaderLen)
		{
			int len = static_cast<uint8_t>(*buf->peek());
			if (buf->readableBytes() < len + kHeaderLen)
			{
				break;
			}
			else
			{
				//1.解析数据,提取id
				int id = static_cast<uint8_t>(buf->peek()[1]);
				id |= (static_cast<uint8_t>(buf->peek()[2]) << 8);
				if (id != 0)
				{
					std::map<int, TcpConnectionPtr>::iterator it = m_mapClientConn.find(id);
					if (it != m_mapClientConn.end())
					{
						//2.转发特定connection
						it->second->send(buf->peek() + kHeaderLen, len);
					}
				}
				else
				{
					//3.如果id=0，则backend通知multiplexerServer断开某个连接
					//规定断开某个connection命令为：DISCONNECT+id
					string cmd(buf->peek() + kHeaderLen, len);
					LOG_INFO << "Backend cmd " << cmd;
					doCommand(cmd);
				}
				buf->retrieve(len + kHeaderLen);
			}
		}
	}

	//根据cmd关闭某个connection
	void doCommand(const string& cmd)
	{
		static const string kDisconnectCmd = "DISCONNECT";

		if (cmd.size() > kDisconnectCmd.size()
			&& std::equal(kDisconnectCmd.begin(), kDisconnectCmd.end(), cmd.begin()))
		{
			int connId = atoi(&cmd[kDisconnectCmd.size()]);
			std::map<int, TcpConnectionPtr>::iterator it = m_mapClientConn.find(connId);
			if (it != m_mapClientConn.end())
			{
				it->second->shutdown();
			}
		}
	}

	void sendBackendString(int id, const string& msg)
	{
		assert(msg.size() <= kMaxPacketLen);
		Buffer buf;
		buf.append(msg);
		sendBackendPacket(id, &buf);
	}

	//每个数据片段有 3 字节的数据头。分别是数据长度一字节和 2 字节的连接 id 号,打包将数据发到backend Server
	void sendBackendPacket(int id, Buffer* buf)
	{
		size_t len = buf->readableBytes();
		LOG_DEBUG << "sendBackendPacket " << len << " bytes";
		assert(len < kMaxPacketLen);
		uint8_t header[kHeaderLen] = {
			static_cast<uint8_t>(len),
			static_cast<uint8_t>(id & 0xFF),
			static_cast<uint8_t>((id & 0xFF00) >> 8)
		};
		buf->prepend(header, kHeaderLen);
		if (m_backendConnn)
			m_backendConnn->send(buf);
	}

	TcpServer						m_server;		//处理外部连接的server
	TcpClient						m_backend;		//将N个TCP连接发送到后端逻辑服务器
	TcpConnectionPtr				m_backendConnn;
	std::map<int, TcpConnectionPtr> m_mapClientConn;
	std::map<string, int>			m_mapConnId;
	std::queue<int>					m_availIds;
};

int main(int argc, char* argv[])
{
	EventLoop loop;
	InetAddress listenAddr(kClientPort);
	if (argc > 1)
	{
		backendIp = argv[1];
	}
	InetAddress backendAddr(backendIp, kBackendPort);
	Singleton<EventLoopThreadPool>::Instance().Init(&loop, 0);
	Singleton<EventLoopThreadPool>::Instance().start();
	MultiplexServer server(&loop, listenAddr, backendAddr);

	server.start();

	loop.loop();
}