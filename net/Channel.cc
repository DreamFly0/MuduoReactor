#include "Channel.h"
#include "EventLoop.h"
#include "../base/logging.h"

#include <poll.h>
#include <sstream>
const int Channel::kNoneEvent = 0;
const int Channel::kReadEvent = POLLIN | POLLPRI;
const int Channel::kWriteEvent = POLLOUT;

Channel::Channel(EventLoop* loop, int fdArg)
  : loop_(loop),
    fd_(fdArg),
    events_(0),
    revents_(0),
    index_(-1),
	addedToLoop_(false),
	eventHandling_(false)	
{
	//LOG_DEBUG << "Channel ctor[" << this << "] fd = " << fd_;
}

Channel::~Channel()
{
	//LOG_DEBUG << "Channel dtor[" << this << "] fd = " << fd_;
	//防止出现Channel对象已经析构的情况下调用handleEvent()
	assert(!eventHandling_);
	assert(!addedToLoop_);
}

void Channel::update()
{
	addedToLoop_ = true;
	loop_->updateChannel(this);
}

void Channel::remove()
{
	assert(isNoneEvent());
	addedToLoop_ = false;
	loop_->removeChannel(this);
}
void Channel::handleEvent(Timestamp receiveTime)
{
	eventHandling_ = true;
	if (revents_ & POLLNVAL)		//文件描述符没有打开
	{
		LOG_WARN << "Channel::handle_event() POLLNVAL";
	}

	if ((revents_ & POLLHUP) && !(revents_ & POLLIN))
	{
		LOG_WARN << "Channel::handle_event() POLLHUP";
		if (closeCallback_) closeCallback_();
	}

	if (revents_ & (POLLERR | POLLNVAL))	//错误
	{
		//LOG_TRACE << "Channel::handleEvent : POLLERR";
		if (errorCallback_) errorCallback_();
	}
	//POLLRDHUP它在socket上接收到对方关闭连接的请求之后触发。使用此事件需要在代码最开始处定义_GNU_SOURCE
	if (revents_ & (POLLIN | POLLPRI | POLLRDHUP))
	{
		//LOG_TRACE << "Channel::handleEvent : POLLIN";
		if (readCallback_) readCallback_(receiveTime);
	}
	
	if (revents_ & POLLOUT)
	{
		//LOG_TRACE << "Channel::handleEvent : POLLOUT";
		if (writeCallback_) writeCallback_();
	}
	eventHandling_ = false;
}

string Channel::eventsToString() const
{
	return eventsToString(fd_, events_);
}

string Channel::eventsToString(int fd, int ev)
{
	std::ostringstream oss;
	oss << fd << ": ";
	if (ev & POLLIN)
		oss << "IN ";
	if (ev & POLLPRI)
		oss << "PRI ";
	if (ev & POLLOUT)
		oss << "OUT ";
	if (ev & POLLHUP)
		oss << "HUP ";
	if (ev & POLLRDHUP)
		oss << "RDHUP ";
	if (ev & POLLERR)
		oss << "ERR ";
	if (ev & POLLNVAL)
		oss << "NVAL ";

	return oss.str().c_str();
}