#include "TcpServer.h"

#include "../base/logging.h"
#include "Acceptor.h"
#include "EventLoop.h"
#include "SocketsOps.h"
#include "../base/singleton.h"
#include "EventLoopThreadPool.h"

#include <functional>

#include <stdio.h>  // snprintf
using namespace std::placeholders;		//bind

TcpServer::TcpServer(EventLoop* loop, const InetAddress& listenAddr, const std::string& nameArg, Option option)
  : m_pLoop(CHECK_NOTNULL(loop)),
    m_strName(nameArg),
    m_strHostPort(listenAddr.toHostPort()),
	m_pAcceptor(new Acceptor(m_pLoop, listenAddr,option)),
	m_bStarted(false),
	m_nextConnId(1)
{
	m_pAcceptor->setNewConnectionCallback(std::bind(&TcpServer::newConnection, this, _1, _2));
}

TcpServer::~TcpServer()
{
	//m_pLoop->assertInLoopThread();
	LOG_INFO << "TcpServer::~TcpServer [" << m_strName << "] dtor";

	for (auto item : m_mapConn)
	{
		TcpConnectionPtr conn = item.second;
		item.second.reset();
		conn->getLoop()->runInLoop(std::bind(&TcpConnection::connectDestroyed, conn));
		
		conn.reset();
	}
}

void TcpServer::start()
{
	if(!m_bStarted) m_bStarted = true;
	
	if(!m_pAcceptor->listenning())
	{
		m_pLoop->runInLoop(std::bind(&Acceptor::listen,m_pAcceptor.get()));
	}
}

void TcpServer::newConnection(int sockfd, const InetAddress& peerAddr)
{
	m_pLoop->assertInLoopThread();
	char buf[32];
	snprintf(buf, sizeof(buf), ":%s#%d", m_strHostPort.c_str(), m_nextConnId);
	if (m_nextConnId == INT32_MAX) m_nextConnId = 0;
	++m_nextConnId;
	std::string connName = m_strName + buf;
	LOG_INFO << "TcpServer::newConnection [" << m_strName
			 << "] - new Connection [" << connName
			 <<"] from " << peerAddr.toHostPort();
	InetAddress localAddr(sockets::getLocalAddr(sockfd));

	EventLoop* ioLoop = Singleton<EventLoopThreadPool>::Instance().getNextLoop();
	//FIXME poll with zero timeout to double confirm the new connection
	//TcpConnectionPtr conn(new TcpConnection(m_pLoop, connName, sockfd, localAddr, peerAddr));
	TcpConnectionPtr conn = std::make_shared<TcpConnection>(ioLoop, connName, sockfd, localAddr, peerAddr);
	m_mapConn[connName] = conn;
	conn->setConnCallback(m_connCallback);
	conn->setMsgCallback(m_msgCallback);
	conn->setWriteCompleteCallback(m_writeCompleteCallback);
	conn->setCloseCallback(std::bind(&TcpServer::removeConnection,this,_1));
	//该线程分离完io事件后，立即调用TcpConnection::connectEstablished
	ioLoop->runInLoop(std::bind(&TcpConnection::connectEstablished, conn));
	
}

void TcpServer::removeConnection(const TcpConnectionPtr& conn)
{
	m_pLoop->runInLoop(std::bind(&TcpServer::removeConnectionInLoop, this, conn));
}

void TcpServer::removeConnectionInLoop(const TcpConnectionPtr& conn)
{
	m_pLoop->assertInLoopThread();
	LOG_INFO << "TcpServer::removeConnection [" << m_strName << "] - connection " << conn->name();
	size_t n = m_mapConn.erase(conn->name());
	assert(1 == n);
	EventLoop* ioLoop = conn->getLoop();
	ioLoop->queueInLoop(std::bind(&TcpConnection::connectDestroyed, conn));
}