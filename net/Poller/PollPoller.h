#pragma once

#include "../Poller.h"

#include <vector>
struct pollfd;

class PollPoller : public Poller
{
public:
	PollPoller(EventLoop* loop);
	virtual ~PollPoller();

	/// Polls the I/O events.
	/// Must be called in the loop thread.
	virtual Timestamp poll(int timeoutMs, ChannelList* activeChannels);

	/// Changes the interested I/O events.
	/// Must be called in the loop thread.
	virtual void updateChannel(Channel* channel);

	/// Remove the channel, when it destructs.
	/// Must be called in the loop thread.
	virtual void removeChannel(Channel* channel);
private:
	void fillActiveChannels(int numEvents,
		ChannelList* activeChannels) const;

	typedef std::vector<struct pollfd> PollFdList;
	PollFdList pollfds_;
	
};