# reactor
s00-s10是一个从零开始构建一个网络库的过程，摘自陈硕linux多线程服务端编程一书
没有用boost库，用C++11重写,net是该网络库的最终版

s00：一个什么都不做的reactor:
只有一个EventLoop,这个类对象的创建遵循one loop per thread原则，用一个threadId_类变量记住本对象所属线程

s01:增加reactor的几个关键结构，完成最核心的事件分发机制：
(1)Channel class:提供用户注册事件回调函数的接口（setXXXCallback）以及注册用户感兴趣的事件（enableXXX），
在handleEvent执行用户的回调函数，生命期由owner class管理
(2)Poller class:封装IO multiplexing: linux poll(后续支持epoll)，Poller不拥有Channel，通过updateChannel和
removeChannel register和unregister相应fd的事件，其维护一个fd到Channel的map，保存关注的fd（文件描述符）,
将poll调用获得的当前活动IO事件交给调用方传入的activeChannels
(3)EventLoop的loop中调用activeChannels的handleEvent函数

//s08 TcpConnection类新增三个接口：
1.发送数据接口send 
2.写入事件handlewrite，如果数据没有一次性发送完，将送入发送缓冲区，触发POLLOUT事件
3.关闭socket上数据的写入shutdown

//s09 增加TcpConnector类，主动发起tcp连接，供TcpClient使用，TcpServer的socket关闭任务由Acceptor析构调用的
//但是这里创建的socket每次都是retry函数关闭又重新创建连接，没懂为什么这么处理
//EventLoop增加SIGPIPE信号处理类
//TcpConnection类新增两个回调：
1.WriteCompleteCallback m_writeCompleteCallback;数据发送完成回调
2.HighWaterMarkCallback m_highWaterMarkCallback;数据发送高水位回调

s10  update:
TcpConnection 增加了一个forceclose接口，用在客户端程序中，其作用相当于handread收到0字节，直接断开tcp连接，清理channel
在测试程序test12.1中让loop停止模拟了这个情况
增加了TcpClient接口，在该类的实现中注意TcpClient析构函数中的实现,因为TcpClient析构之后，注册的setCloseCallback
函数不能调用了，所以重新注册了一个close回调函数

2018/8/2 net update:
1.增加EventLoopThreadPool线程池类，修改TcpSever支持多线程ioLoop,newConnection函数中
通过round-robin(轮询调度)的方式获取每个TcpConnection的ioLoop,同时修改TcpSever的connnecedDestory函数调用的
io线程

