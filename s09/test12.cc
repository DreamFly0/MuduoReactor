#include "Connector.h"
#include "EventLoop.h"
#include "../base/logging.h"

#include <stdio.h>
#include <memory>

EventLoop* g_loop;
typedef std::shared_ptr<Connector> ConnectorPtr;

void connectCallback(int sockfd)
{
	printf("connected.\n");
	g_loop->quit();
}

int main(int argc, char* argv[])
{
	Logger::setLogLevel(Logger::TRACE);
	EventLoop loop;
	g_loop = &loop;
	InetAddress addr("127.0.0.1", 1111);
	ConnectorPtr connector(new Connector(&loop, addr));
	connector->setNewConnectionCallback(connectCallback);
	connector->start();

	loop.loop();
}